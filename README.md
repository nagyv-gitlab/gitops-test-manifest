# GitOps Test Manifest

This repo is an example setup to deploy application code using the GitLab Kubernetes Agent. It deploys the application code from https://gitlab.com/nagyv-gitlab/gitops-test. This project contains the kubernetes manifest files as they are being deployed into various environments.

For further details, please read the README of the https://gitlab.com/nagyv-gitlab/gitops-test repository.
